import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AssistantTest {

    @Test
    void save_luggage_by_assistant_return_ticket() {
        Storage storage = new Storage();
        Assistant assistant = new Assistant(storage);
        Luggage luggage1 = new Luggage("luggage1");
        Ticket ticket = assistant.save(luggage1);

        assertNotNull(ticket);
    }
}
