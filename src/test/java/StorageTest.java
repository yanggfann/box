import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StorageTest {
    private Storage storage;
    private Luggage luggage;

    //存一个，得到ticket
    @Test
    void test_storage() {
        storage = new Storage();
        luggage = new Luggage("luggage");

        Ticket ticket = storage.save(luggage);

        assertNotNull(ticket);
    }

    //凭ticket取luggage
    @Test
    void test_get_luggauge() {
        storage = new Storage();
        luggage = new Luggage("luggage");
        Ticket ticket = storage.save(luggage);

        assertEquals("luggage", storage.get(ticket).getName());
    }

    //存空值，取空值
    @Test
    void test_get_null() {
        storage = new Storage();
        luggage = new Luggage(null);
        Ticket ticket = storage.save(luggage);

        assertNull(storage.get(ticket).getName());
    }

    //存两个luggage,取第一个
    @Test
    void test_get_first_luggage() {
        storage = new Storage();
        Luggage luggage1 = new Luggage("luggage1");
        Luggage luggage2 = new Luggage("luggage2");
        Ticket ticket1 = storage.save(luggage1);
        Ticket ticket2 = storage.save(luggage2);
        assertEquals("luggage1", storage.get(ticket1).getName());
    }

    //设置storage的大小，超出报异常信息并捕获
    @Test
    void test_out_of() {
        luggage = new Luggage("luggage1");
        storage = new Storage(1);
        storage.save(luggage);
        Luggage luggage2 = new Luggage("luggage2");

        try{
            storage.save(luggage2);
        }catch (RuntimeException e){
            assertEquals("out of capacity", e.getMessage());
        }
    }

}
