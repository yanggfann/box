import java.util.*;

public class Storage {

    private List<Slot> allSlots;
    private Map<Ticket, Slot> slots;
    private int size;

    public Storage() {
        this(10);
    }

    public Storage(int size) {
        this.size = size;
        this.allSlots = initAllSlots(size);
        this.slots = new HashMap<>();
    }

    private ArrayList<Slot> initAllSlots(int size) {
        return new ArrayList<Slot>() {{
            for(int i = 0; i < size; i++){
                add(new Slot());
            }
        }};
    }

    private int getSize() {
        return size;
    }

    public Ticket save(Luggage luggage){
        Ticket ticket = new Ticket();
        if(slots.size() == getSize()){
            throw new RuntimeException("out of capacity");
        }
        
        Slot slot = firstAvailableSlot();
        slot.setLuggage(luggage);
        this.slots.put(ticket, slot);
        return ticket;
    }

    private Slot firstAvailableSlot() {
        return this.allSlots.stream()
                .filter(Slot::isEmpty)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("out of capacity"));
    }


    public Luggage get(Ticket ticket) {
        return this.slots.remove(ticket).getLuggage();
    }
}
