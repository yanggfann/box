public class Luggage {
    private String name;

    public Luggage() {
    }

    public Luggage(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
