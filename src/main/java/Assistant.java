public class Assistant {
    private Storage storage;

    public Assistant(Storage storage) {
        this.storage = storage;
    }

    public Ticket save(Luggage luggage) {
        return storage.save(luggage);
    }

    public Luggage get(Ticket ticket){
        return storage.get(ticket);
    }
}
