import java.util.Objects;

public class Slot {
    private int id;
    private Luggage luggage;
    private int size;

    public Slot() {
    }

    public Slot(Luggage luggage) {
        this.luggage = luggage;
    }

    public Slot(int id, Luggage luggage, int size) {
        this.id = id;
        this.luggage = luggage;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public Luggage getLuggage() {
        return luggage;
    }

    public int getSize() {
        return size;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLuggage(Luggage luggage) {
        this.luggage = luggage;
    }

    public void setSize(int size) {
        this.size = size;
    }

    boolean isEmpty() {
        return Objects.equals(getLuggage(), null);
    }
}
